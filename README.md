[![pipeline status](https://gitlab.com/vochr/rBDATPRO/badges/master/pipeline.svg)](https://gitlab.com/vochr/rBDATPRO/commits/master)    
[![coverage report](https://gitlab.com/vochr/rBDATPRO/badges/master/coverage.svg)](https://gitlab.com/vochr/rBDATPRO/commits/master)
<!-- 
    [![Build Status](https://travis-ci.org/fvafrCU/rBDATPRO.svg?branch=master)](https://travis-ci.org/fvafrCU/rBDATPRO)
    [![Coverage Status](https://codecov.io/github/fvafrCU/rBDATPRO/coverage.svg?branch=master)](https://codecov.io/github/fvafrCU/rBDATPRO?branch=master)
-->
[![CRAN_Status_Badge](https://www.r-pkg.org/badges/version/rBDATPRO)](https://cran.r-project.org/package=rBDATPRO)
[![RStudio_downloads_monthly](https://cranlogs.r-pkg.org/badges/rBDATPRO)](https://cran.r-project.org/package=rBDATPRO)
[![RStudio_downloads_total](https://cranlogs.r-pkg.org/badges/grand-total/rBDATPRO)](https://cran.r-project.org/package=rBDATPRO)

<!-- README.md is generated from README.Rmd. Please edit that file -->



# rBDATPRO
## Installation
You can install rBDATPRO from gitlab via:


```r
if (! require("remotes")) install.packages("remotes")
remotes::install_gitlab("vochr/rBDATPRO", build_vignettes = TRUE)
```

## Introduction
After installation, see the help page:

```r
help("rBDATPRO-package", package = "rBDATPRO")
```

```
#> Implementation of BDAT Tree Taper Fortran Functions in R
#> 
#> Description:
#> 
#>      Implementing the BDAT tree taper Fortran routines to be available
#>      in R to calculate diameters, volume, assortments, double bark
#>      thickness and biomass for different tree species based on tree
#>      characteristics and sorting information.
```

or read the vignette 

```r
vignette("rbdatpro", package = "rBDATPRO")
```


