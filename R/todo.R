#' todo:
#' Remember: BDATVOLDHMR always uses SekLng = 2, hard-coded in Fortran, see
#' function xFNBDATVolDHmR
#'
#' BDAT-R-BDATVOLDHMR internally calls BDATVOLABMR, with A=0.
#' BDATVOLDHMR has parameter HDHGrz, which is not required (fortran return value)
#' BDATVOLDHMR has parameter DHGrz, which is awkward naming, since it means
#' DerbholzGrenze, which is not what is meant, since its a parameter!
#' Function only kept for back-compatibility
#'
#'
#' FINALLY: check which LICENSE need to be given to the package
#' !DONE!
#'
#' eventually there is an error in the amount of INdustrieholz:
#' Länge ist Höhe Zopf Abschnitt + 1% Länge Abschnitt, aber nicht mindestens 10cm.
#' vgl. Zeile 147 der subroutine Sortmi (Mittendurchmesser, d.h. Sokz=1) bzw.
#' Zeile 233 in Subroutine Sorthl (heilbronner sortierung)
#' das gleiche gilt für die Stammholzstücke (Zugabe 1% der Länge)
#'
#' problem in getAssortment (d.h. in BDAT20)
#' X-Holz wert in LDSort u.U falsch. Zwar ist länge =0, aber anfangshöhe, midD und topD != 0
#' je nach rückgabe-wert (parameter value) sichtbar. insbesondere bei
#' value ="LDSort" und "merge" (dieser verschmelzt LDSort mit Vol)
#' !DONE! > v0.6.2
#'
#' R-Abgestürzt nach aufruf von
#' rBDATPRO::getDiameter(trees[1], Hx = 0.1111228+1, bark = F)
#' wobei trees:
#' > str(trees)
# Classes ‘datBDAT’ and 'data.frame':	36 obs. of  6 variables:
#   $ spp: num  1 1 1 1 1 1 1 1 1 1 ...
# $ D1 : num  10 12 14 16 18 20 22 24 26 28 ...
# $ H1 : num  0 0 0 0 0 0 0 0 0 0 ...
# $ D2 : num  0 0 0 0 0 0 0 0 0 0 ...
# $ H2 : num  0 0 0 0 0 0 0 0 0 0 ...
# $ H  : num  11.1 13.4 15.6 17.5 19.3 ...
## cleared error when wrongly calling bdat routines with class attribute but
## with malicious class object. Now error thrown and no call to fortran with
## bad data
#' !DONE! > v0.6.1
#'
#' in buildTree checks require that requested diameter or height are within
#' specified tree. But it is not necessary, hx can be higher than tree height
#' (but not below zero)
#' correct for that, because its an anoying error that Hx > H!
#' !DONE!
#'
#' plot.datBDAT: Farbe von Rinde falsch herum zugeordnet.
#' !DONE!
#'
#' Bäume mit BHD < 10 werden ins industrieholz sortiert, aber keine Länge und MDM
#' !DONE! > v0.6.3
#'
#' getAssortment(list(spp=1, D1=30, H=25), sort = list(fixL=4, fixN=30))
#' liefert eine falsche Höhe des nvDh segments
#' Hat wohl was mit H0fixLng zu tun. siehe Zeile 979 und 982 BDATpro.f
#' !Done! > v0.6.4

#' Idee: in plot-function ergänze eine Sortennamen-Spalte, welche priorität
#' in die SortenStücke geschrieben wird (stat der "Sort"-Spalte)
#' !DONE! (use colume 'assortname')

#' falscher Durchmesser für nvDh in einem Fall erzeugt, Kälberbronn-Daten,
#' 2m Fixlängen gehen fast bis 7cm Az, evtl. nvDh < 1m, dann falsch???
#'
#' getDiameter anpassen, sodass Hx > H OK ist.
#' !DONE!
#'
#' feature beschreiben, dass in getdiameter Hx im tree-object sein kann oder als
#' extra parameter angegeben werden kann, und falls dies ein vector mit länge >1
#' ist, dieses Hx an jeden Datensatz in tree drangeschrieben wird.
#' !DONE! => vignette
#'
#' Diese Feature gibts nicht in getHeight, wäre aber konsistent!
#' !DONE!
#'
#' getVolume enhanced to independently specify bark attribute for input diameter
#' and ouput volume
#' !DONE!
#'
#' if in getHeight parameter Dx is given as a list (Dx=list(Dx=c(7, 5))) then
#' no full outer join is produced. same for getDiameter
#' !DONE! for all functions > v0.7.0
#'
#' check rBDATPRO::getAssortment(list(spp=1, D1=5, H=15))
#' ifeh=13 (kein Massentafelwert vorhanden)
#' => fälle, bei denen ifeh!=0 zurücksetzen und warning/error werfen
#'
#' evtl. macht BDAT einen kleinen Fehler an der Durchmesser Grenze für den
#' Rindenabzug, denn in BDAT wird zuerst der Rindenabzug realisiert und dann
#' der Durchmesser abgerundet. Im Wald findet dies in umgekehrter Reihenfolge
#' statt (RL gibt forstlich gerundeten Durchmesser mit Rinde an, Rinde wird
#' z.B. in Fokus vom abgerundeten Durchmesser abgezogen)
#'
#' Klassenattribute werden eigentlich mit structure(x, class="class-name") übergeben
#'
#' die in BDAT implementierten Funktionen sind nicht klassenspezifisch, d.h. man
#' kann damit jegliches Objekt verarbeiten, egal welcher Klasse
